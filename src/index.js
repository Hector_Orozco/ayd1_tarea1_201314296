const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

// creacion de la app o mi API
const app = express();




//configuraciones
app.set('port',3000);
app.use(express.json())
app.use(cors());


//routes
app.get('/',(req,res)=>{
    res.send('Hola mundo desde mi primer Backend con NodeJS');
});

app.use('/',require('./routers/suma'));
app.use('/',require('./routers/info'));
app.use('/',require('./routers/resta'));
app.use('/',require('./routers/division'));


// usando morgan para middlewares
app.use(morgan('dev')); // para poder visualizar los estados de nuestro servidor
app.use(express.json()); // para poder manjar los json
app.use(cors());


// inicializando mi servidor
app.listen(app.get('port'),()=>{
    console.log('Servidor iniciado en el puerto: '+app.get('port'));
})