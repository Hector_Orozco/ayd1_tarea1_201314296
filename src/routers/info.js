const { Router } = require('express');
const router = Router();


router.get('/info',(req,res)=>{
    res.status(200).send({
        nombre: 'Hector Orozco',
        carnet: 201314296
    });
});


module.exports = router;