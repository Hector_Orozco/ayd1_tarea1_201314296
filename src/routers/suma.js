const { Router } = require('express');
const router = Router();

router.post('/suma',(req,res)=>{
    let dato1 = req.body.dato1;
    let dato2 = req.body.dato2;
    let suma = dato1 + dato2;
    res.status(200).send({
        suma: suma
    });
});

module.exports = router;