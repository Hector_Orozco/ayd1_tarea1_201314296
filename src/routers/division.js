const { Router } = require('express');
const router = Router();



router.post('/division',(req,res)=>{
    let dato1 = req.body.dato1;
    let dato2 = req.body.dato2;
    let resultado = dato1 / dato2;
    res.status(200).send({
        division: resultado
    });
});


module.exports = router;